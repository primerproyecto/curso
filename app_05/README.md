```shell
**Jorge Alarcón de Mena**
jorge.alarcon@entelgy.com
```

# FORMULARIOS Y VALIDACIONES:
Los formularios es una pieza fundamental dentro de una aplicación web y también una fuente habitual de problemas. Angularjs facilita mucho la vida a la hora de trabajar con ellos.

La unión de atributos en los tag de campos de formulario con el conjunto `ng-messages`/`ng-message` permite un control estricto y preciso.

Hay muchas validaciones disponibles para usarlas "tal cual" (consulta la documentación) y puedes crear otras personalizadas para que se ajusten a tus necesidades, por ejemplo, validar un NIF o un número par menor que 100 y mayor que 20.

# EJERCICIO: 
Ejercicio libre. No hay objetivos ni solución en esta práctica, qué suerte tienes. Crea una nueva validación, ajusta el formulario, crea nuevos campos, utiliza clases de Bootstrap, etc. Aprovecha este tiempo para ver cómo Angularjs se ocupa de las cosas según escribes en los campos. Investiga y prueba opciones. Aquí te pego un par de enlaces de la documentación oficial: `https://docs.angularjs.org/guide/forms` y `https://docs.angularjs.org/api/ngMessages/directive/ngMessages`.