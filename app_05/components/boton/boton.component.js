(function(angular) {

    /**
    * @ngdoc directive
    * @name component.boton
    * @description
    * Componente para boton
    */
    angular
        .module('boton', [])
        .component('boton', {
            templateUrl: 'components/boton/boton-layout.html',
            transclude: true,
            controller: BotonController,
            bindings: {
                onLoad: '&'
            }
        });

    //BotonController.$inject = [];
    /**
    * @ngdoc method
    * @name BotonController
    * @methodOf component.botón
    * @description
    * Controlador para el componente botón
    */
    function BotonController() {
        var ctrl = this;
    }
        
})(window.angular);
    