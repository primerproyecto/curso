// jscs:disable
(function(angular) {
    'use strict';
    /**
    * @ngdoc overview
    * @name app
    * @description
    * Módulo principal que carga las dependencias
    */
    angular
    .module('app', [
        'app.core'
    ]).config(function($locationProvider, $httpProvider) {
    //    $locationProvider.html5Mode(true);
    })
    .run(function($state) {
        $state.go('mainapp');
    });
})(window.angular);

