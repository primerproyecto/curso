(function(angular) {
    'use strict';
    /**
    * @ngdoc overview
    * @name app.core
    * @description
    * Establece el núcleo de la aplicación con las dependencias principales
    *
    */
    angular.module('app.core', [
        'ui.router',
        'ngSanitize',
        'ngResource',
        'ngMessages',
        'ui.bootstrap',
        'pascalprecht.translate',
        'ngCookies',
        'libro',
        'titulo',
        'listado',
        'boton',
        'formulario',
        'listaLibros'

    ]);
})(window.angular);