(function(angular) {
    angular
    .module('formulario')
        .directive('numeric', numeric)
        .directive('alfabetic', alfabetic);

        numeric.$inject = ['configApp'];
        function numeric (configApp) {
            return {
                restrict: 'A',
                require : 'ngModel',
                link: function(scope, elm, attrs, ctrl) {
                    ctrl.$validators.numeric = function(modelValue, viewValue) {
                        if (configApp.regExp.NUMERIC_REGEXP.test(viewValue) || !viewValue) {
                            return true;
                        } else {
                            return false;
                        }
                    };
                }
            };
        }
        alfabetic.$inject = ['configApp'];
        function alfabetic (configApp) {
            return {
                restrict: 'A',
                require : 'ngModel',
                link: function(scope, elm, attrs, ctrl) {
                    ctrl.$validators.alfabetic = function(modelValue, viewValue) {
                        if (configApp.regExp.NONUMBERS_REGEXP.test(viewValue) || !viewValue) {
                            return true;
                        } else {
                            return false;
                        }
                    };
                }
            };
        }

})(window.angular);