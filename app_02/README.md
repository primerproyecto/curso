```shell
**Jorge Alarcón de Mena**
jorge.alarcon@entelgy.com
```
# BOOTSTRAP DE LA APLICACIÓN Y COMPONENTES BÁSICOS

## BOOTSTRAP DE LA APLICACIÓN
Lo que viste en la unidad anterior está bien para empezar, pero, en realidad, no tiene nada que ver con lo que sería una aplicación real. No tiene mucho sentido avanzar en esa dirección.

Una aplicación tiene que hacer algo de trabajo sucio antes de estar disponible. Esto se hace durante el bootstrap, el "encendido". Verás que sin esfuerzo aparente ya estamos viendo algunas cosas llamativas. Las veremos con detalle en las próximas unidades, de momento, quédate con la idea y entiende el proceso.

En el directorio `config` tienes el archivo `app.config.js` que es el responsable de hacer estas cosas de las que estamos hablando. Echa un vistazo a los comentarios en linea para saber qué hace en cada paso.

## MÓDULO APP y CORE
El módulo `app.module.js` representa el módulo principal donde se cargan las dependencias. Podríamos haber inyectado todos los módulos en el principal `app`, pero es preferible mantenerlo lo más limpio posible e incluir en él nada más que los módulos de aplicación principales. Dado el alcance de estos ejemplos no vamos a poder percibir su verdadero poder, pero es una forma genial de organizar tu código cuando tienes varias unidades semánticas.

##COMPONENTES BÁSICOS
A partir de la versión 1.5.0 Angularjs permite crear componentes al modo de `Polymer`, `Reactjs`, etc. En realidad, un `<input>` es un componente web. Lo que hacemos es crearlos **personalizados**, con las características que nos gusten y necesitemos. Y como son componentes los creamos una sola vez y los utilizamos tantas como querramos.

En la carpeta `titulo` tienes un ejemplo muy básico de componente. Los componentes encapsulan lógica, no comparten scope (entendido aquí como encapsulamiento de datos) y tienen un comportamiento autónomo y reutilizable.

# EJERCICIO: 
Simplemente crea otro componente de tipo párrafo, por ejemplo. Es muy fácil.
