    (function(angular) {
    'use strict';
    /**
    * @ngdoc property
    * @name app.config.language
    * @requires $translateProvide, $translatePartialLoaderProvider, config
    * @description
    * Define la configuración del lenguaje
    */

    angular
        .module('app')
        .config(configLanguage);

    configLanguage.$inject = ['$translateProvider', '$translatePartialLoaderProvider', 'configApp'];

    function configLanguage($translateProvider, $translatePartialLoaderProvider, configApp) {
        $translateProvider.useLocalStorage();
        $translateProvider.useSanitizeValueStrategy('sanitizeParameters');
        $translateProvider.fallbackLanguage('es');

        $translatePartialLoaderProvider.addPart('i18n/');
        $translateProvider.useLoader('$translatePartialLoader', {
            urlTemplate: configApp.junctionI18n + '{part}locale-{lang}.json'
        });

        $translateProvider.preferredLanguage(configApp.idioma);
    }
})(window.angular);