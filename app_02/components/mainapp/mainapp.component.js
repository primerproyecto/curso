/* globals _ENV_ */
(function(angular) {

    /**
    * @ngdoc directive
    * @name component.mainapp
    * @description
    * Componente para mainapp
    */
    angular
        .module('app')
        .component('mainapp', {
            templateUrl: 'components/mainapp/mainapp-layout.html',
            controller: MainappController,
        });

    /**
    * @ngdoc method
    * @name incorporacionController
    * @methodOf component.mainapp
    * @description
    * Controlador para el componente mainapp
    */
    function MainappController() {
        var ctrl = this;
    }
        
})(window.angular);
    