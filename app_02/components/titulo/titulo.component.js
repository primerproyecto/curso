/* globals _ENV_ */
(function(angular) {

    /**
    * @ngdoc directive
    * @name component.titulo
    * @description
    * Componente para la incorporación de los ficheros
    */
    angular
        .module('titulo', [])
        .component('titulo', {
            templateUrl: 'components/titulo/titulo-layout.html',
            transclude: true,
            controller: TituloController,
        });

   /**
    * @ngdoc method
    * @name TituloController
    * @methodOf component.titulo
    * @description
    * Controlador para el componente titulo
    */
    function TituloController() {
        var ctrl = this;
    }
        
})(window.angular);
    