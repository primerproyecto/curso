```shell
**Jorge Alarcón de Mena**
jorge.alarcon@entelgy.com
```
# INTRODUCCIÓN

En esta primera práctica vamos a tratar del scope. El scope no es otra cosa que un **objeto que contiene el modelo de una aplicación** remedando su estructura DOM. Parece más complicado que lo que es, piensa en un repositorio de datos cuyo ámbito es el controlador que lo contiene; es decir, el scope no tiene entidad universal, entendiendo esto como ámbito global. Para esto tienes el rootScope, que sí contiene los datos de toda la aplicación disponibles desde cualquier sitio. Olvídate de él, aunque intuitivamente puede parece el últil (como `REDUX`, podrías pensar, salvo que `REDUX` crea copias clones para garantizar la consistencia. Osea, nada quie ver), lo cierto es que es una pesadilla. Lo repetiremos muchas veces, pero alguna vez tiene que ser la original, los datos los manipula el propietario, no los servicios relacionados. Los datos bajan, las acciones suben, nadie más que el origen de los datos debe tocar nada para evitar inconsistencias. Si has pensado en utilizarlo, te equivocas. De hecho, verás que ni siquiera utilizarás scope por mucho tiempo (con algún matiz). Como todo en la vida, el camino fácil es el peor porque es el más transitado y solo conduce a la mediocridad.

Lo que vamos a hacer es muy sencillo. Simplemente vamos a listar unos escritores y vamos a crear un formulario muy sencillo para introducir nuevos. No los vamos a persistir en ningún lado porque lo que nos interesa es trabajar con el scope.

Lo primero que verás es que hay muchos directorios y casi todos vacíos. El motivo es que estas unidades temáticas se elaboraron con el repositorio bootstrap-entelgy que es, precisamente, el que se recomienda en la guía de arquitectura a la que tienes acceso desde su **readme**.

Angularjs es un tipo especial de MVC. El motivo es que, al contrario que frameworks como **Emberjs**, no tiene un modelo propiamente dicho de la forma en la que estamos acostumbrados, tablas, relaciones, etc. Lo veremos de muchas maneras distintas, con otros nombres y relaciones, pero, siempre, en último caso, tenemos la pareja página html (vista) y javascript (controlador).

Nuestro html en este sencillo ejemplo está en el `index.html` y el controlador en `controllers.js`. Lo más importante es recordar siempre que para que esté disponible un recurso es preciso insertarlo en nuestra página html. Busca en el index.html `<script src="controllers/controller.js"></script>`.

En el html verás un bucle que itera sobre una colección `modelo` que está declara en `controller.js'. Cada campo de formulario debe tener un `ng-model` que lo relacione con el `$scope`. También tenemos un botón submit que lanza el método `registrar`.

Verás que el listado se refresca sin necesidad de hacer absolutamente nada cuando se añade un nuevo escritor. Angularjs lo hace por nosotros entre bambalinas. Esta es una de las características que justifican el uso de un framework, la consistencia de las fuente de datos y de los módulos que hagan uso de ello.

## EJERCICIO: 
El ejercicio que te propongo es sencillo. Crea un método que elimine escritores. Necesitarás un botón al lado de cada uno de la lista para que se pueda elegir. Con `$index` podrás pasarle el lugar que ocupa al nuevo método del scope. Con `splice`, que es un método del objeto nativo `array`de javascript, podrás eliminar los objetos. Mira cómo se utiliza aquí: `https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/splice`.

