/* globals _ENV_ */
(function(angular) {

    /**
    * @ngdoc directive
    * @name component.incorporacion
    * @description
    * Componente para la incorporación de los ficheros
    */
    angular
        .module('app')
        .component('mainapp', {
            templateUrl: 'components/mainapp/mainapp-layout.html',
            controller: MainappController,
        });

    /**
    * @ngdoc method
    * @name incorporacionController
    * @methodOf component.incorporacion
    * @description
    * Controlador para el componente incorporación
    */
    MainappController.$inject = ['dataservice', 'configApp'];
    function MainappController(dataservice, configApp) {
        var ctrl = this;

        //Método para cargar el contenido tras interacción con el botón de la vista
        ctrl.loadContent = function(){
            //inicializamos 'data', como no es necesario mandar nada al servidor, esta variable queda
            //vacía
            var data = '';
            //Invocación al servicio que hemos inyectado para la carga de los datos haciendo uso del objeto
            //de configuración
            dataservice.ajaxRequest(_ENV_.mainApp.url, _ENV_.mainApp.method, data, configApp).then(function(response) {
            //Asignamos la colección a la variable sobre la que iteraremos.
                ctrl.libros = response.data.libros;
            },
            function(response) {
                console.log('error');
            });
        }
    }
})(window.angular);
    