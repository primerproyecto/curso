```shell
**Jorge Alarcón de Mena**
jorge.alarcon@entelgy.com
```
# app_0401: INYECCIÓN DE DEPENDENCIAS, SERVICIOS PROMISE, CARGA DE DATOS, ITERACIÓN DE DATOS

En esta primera parte de la cuarta unidad iteramos sobre una colección que obtenemos en el componente `mainapp`.

Inyectamos nuestro servicio de datos por medio del cual vamos a consultar todos los datos de la aplicación mendiante un sistema `promise`. También verás que cargamos el archivo de configuración que creamos en el bootstrap de la aplicación. En él definimos las junction a nuestros servicios de datos, de manera que si cambiaran por el motivo que fuera o quisiéramos reutilizar código, podríamos hacerlo. No es complicado, pero son muchos archivos y muchos detalles, presta atención.

Con esos datos iteramos la colección y los presentamos en pantalla haciendo uso de las clases por defecto que Bootstrap pone a nuestra disposición sin más intervención humana.

# app_0402: REFACTORIZACIÓN DEL COMPONENTE LISTADO

En este apartado creamos un componente `listado` al que pasamos por parámetro nuestras inyecciones de dependencia.

# EJERCICIO
Ahora toca limpiar y refactorizar el código. Crea un componente `listado`, dentro de él, el botón, un `ul` y un `li` separados en sendos componentes. No es sencillo, intenta por lo menos plantearlo sobre papel. Crea también un componente botón. Esto es más fácil, lo hemos visto a través de las anteriores lecciones.

Si crees que andas cerca, puedes ver la solución en la unidad `app_0402_solucion`. Las inyecciones de dependencia es un método habitual en la modularización y reutilización de código. Angularjs hace uso constante de ese concepto. Aquí lo utilizamos para varias cosas.

Es importante que entiendas esto. Los componentes y sus relaciones son la clave en una aplicación Angularjs bien estructurada.

