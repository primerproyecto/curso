/* globals _ENV_ */
(function(angular) {

    /**
    * @ngdoc directive
    * @name component.lista-group
    * @description
    * Componente agrupoación de listado de libros
    */
    angular
        .module('listaLibros')
        .component('listaElemento', {
            require: {
                ListaGrupoCtrl: '^listaGrupo'
            },
            templateUrl: 'components/lista-elemento/lista-elemento-layout.html',
            controller: ListaElementoController
        });

    /**
    * @ngdoc method
    * @name ListaGrupoController
    * @methodOf component.listado
    * @description
    * Controlador para el componente listado
    */   
    ListaElementoController.$inject = [];
    function ListaElementoController(){
        var ctrl = this;

        //Con require hemos creado una dependencia con un componente padre de manera
        //que tenemos acceso a los datos del padre.

        //Una asignación simple no funcionaría puesto que no sabemos en qué momento estarán
        //disponibles en el hijo los datos del padre. Por ello utilizamos '$onInit'
        ctrl.$onInit = function() {
            ctrl.libros = ctrl.ListaGrupoCtrl.libros;
        };
    }

})(window.angular);