(function(angular) {
    'use strict';
    /**
    * @ngdoc service
    * @name app.service:dataservice
    * @requires $http, $resource
    * @description
    * Servicio la gestión de peticiones asíncronas
    */
    angular
        .module('app.core')
        .factory('dataservice', dataservice);

    dataservice.$inject = ['$http', '$resource', '$q'];

    function dataservice($http, $resource, $q) {
        var service = {
            getMainData: getMainData,
            getSubMainData: getSubMainData,
            ajaxRequest: ajaxRequest
        };
        return service;

        /**
        * @ngdoc method
        * @name ajaxRequest
        * @methodOf app.service:dataservice
        * @requires $http, $resource
        * @description
        * Centraliza las llamadas ajax de la aplicación
        */
        function ajaxRequest(url, method, data, config) {
            var defered = $q.defer();
            var promise = defered.promise;
            if (method.toUpperCase() === 'POST') {
                $http.post(config.urlRoot + url, data, config).then(getDataComplete, getDataFailed);
            } else if (method.toUpperCase() === 'PUT') {
                $http.put(config.urlRoot + url, data, config).then(getDataComplete, getDataFailed);
            } else if (method.toUpperCase() === 'DELETE') {
                $http.delete(config.urlRoot + url, config).then(getDataComplete, getDataFailed);
            } else { //Por defecto GET
                $http.get(config.urlRoot + url, config).then(getDataComplete, getDataFailed);
            }
            return promise;

            function getDataComplete(data) {
                defered.resolve(data.data);
            }

            function getDataFailed(error) {
                defered.reject(error.data);
            }
        }

        function getMainData() {
            return $http.get('/json/main.json')
                .then(getDataComplete)
                .catch(getDataFailed);

            function getDataComplete(data, status, headers, config) {
                return data.data;
            }

            function getDataFailed(error) {
                console.log('XHR ha fallado para MAIN.' + error.data);
            }
        }

        function getSubMainData() {
            return $http.get('/json/main.json')
                .then(getDataComplete)
                .catch(getDataFailed);

            function getDataComplete(data, status, headers, config) {
                return data.data;
            }

            function getDataFailed(error) {
                console.log('XHR ha fallado para MAIN.' + error.data);
            }
        }
    }
})(window.angular);
