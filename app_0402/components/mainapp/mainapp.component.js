/* globals _ENV_ */
(function(angular) {

    /**
    * @ngdoc directive
    * @name component.incorporacion
    * @description
    * Componente para la incorporación de los ficheros
    */
    angular
        .module('app')
        .component('mainapp', {
            templateUrl: 'components/mainapp/mainapp-layout.html',
            controller: MainappController,
        });

    /**
    * @ngdoc method
    * @name incorporacionController
    * @methodOf component.incorporacion
    * @description
    * Controlador para el componente incorporación
    */
    MainappController.$inject = [];
    function MainappController() {
        var ctrl = this;
        //Método para cargar el contenido tras interacción con el botón de la vista
    }
})(window.angular);
    