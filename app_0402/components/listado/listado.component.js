/* globals _ENV_ */
(function(angular) {

    /**
    * @ngdoc directive
    * @name component.listado
    * @description
    * Componente principal para la gereración del listado de libros
    */
    angular
        .module('listado', [])
        .component('listado', {
            templateUrl: 'components/listado/listado-layout.html',
            controller: ListadoController,
        });

    /**
    * @ngdoc method
    * @name ListadoController
    * @methodOf component.listado
    * @description
    * Controlador para el componente listado
    */   
    ListadoController.$inject = ['$injector', '$attrs'];
    function ListadoController($injector, $attrs){
        var ctrl = this;
        ctrl.dataservice = $injector.get($attrs.dataservice);
        ctrl.configApp = $injector.get($attrs.configApp);

        ctrl.loadContent = function(){
            //inicializamos 'data', como no es necesario mandar nada al servidor, esta variable queda
            //vacía
            var data = '';

            //(pensemos en encapsular este método dentro de un servio que lo aplique, de manera que la implementación particular de la llamada
            //no suponga una dependencia externa)
            //Invocación al servicio que hemos inyectado para la carga de los datos haciendo uso del objeto
            //de configuración
            ctrl.dataservice.ajaxRequest(_ENV_.mainApp.url, _ENV_.mainApp.method, data, ctrl.configApp).then(function(response) {
            //Asignamos la colección a la variable sobre la que iteraremos.
                ctrl.libros = response.data.libros;
            },
            function(response) {
                console.log('error');
            });
        }
    }
})(window.angular);