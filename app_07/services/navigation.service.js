(function(angular) {
    'use strict';
    /**
    * @ngdoc service
    * @name app.service:dataservice
    * @requires $state
    * @description
    * Servicio para la gestión de la navegación
    */
    angular
        .module('app.core')
        .factory('navigation', navigation);

    navigation.$inject = ['$state', '$location'];

    function navigation ($state, $location) {
        var service = {
            goNextState: goNextState,
            getClass: getClass
        };
        return service;

        /**
        * @ngdoc method
        * @name ajaxRequest
        * @methodOf app.service:navigation
        * @description
        * Centraliza la navegación de la aplicación
        */
        function goNextState(whereToGo) {
           switch (whereToGo) {
                case 'mainapp': 
                    $state.go('mainapp');
                    break;
                case 'about': 
                    $state.go('about');
                    break;
                default:
                    $state.go('mainapp');
            }
        }

        /**
        * @ngdoc method
        * @name getClass
        * @methodOf app.service:navigation
        * @description
        * Devuelve si true/false para la clase activa
        */
        function getClass(path) {
            return ($location.path().indexOf(path) > -1) ? 'active' : '';
        }
    }
})(window.angular);
