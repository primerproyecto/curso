/* globals _ENV_ */
(function(angular) {

    /**
    * @ngdoc directive
    * @name component.lista-group
    * @description
    * Componente agrupoación de listado de libros
    */
    angular
        .module('listaLibros')
        .component('listaElemento', {
            require: {
                ListaGrupoCtrl: '^listaGrupo'
            },
            templateUrl: 'components/lista-elemento/lista-elemento-layout.html',
            controller: ListaElementoController
        });

    /**
    * @ngdoc method
    * @name ListaGrupoController
    * @methodOf component.listado
    * @description
    * Controlador para el componente listado
    */   
    ListaElementoController.$inject = ['$scope', '$injector', '$attrs'];
    function ListaElementoController($scope, $injector, $attrs){
        var ctrl = this;
        ctrl.$onInit = function() {
            ctrl.libros = ctrl.ListaGrupoCtrl.libros;
        };

        $scope.$watch(angular.bind(ctrl.ListaGrupoCtrl, function() {
            return ctrl.ListaGrupoCtrl.filterLibros;
        }), function(value) {
            ctrl.query = value; 
        });
    }

})(window.angular);