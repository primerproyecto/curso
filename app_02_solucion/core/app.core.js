(function(angular) {
    'use strict';
    /**
    * @ngdoc overview
    * @name app.core
    * @description
    * Establece el núcleo de la aplicación con las dependencias principales
    *
    */
    angular.module('app.core', [
        'ui.router',
        'ngSanitize',
        'ngResource',
        'ui.bootstrap',
        'pascalprecht.translate',
        'ngCookies',
        'titulo'
    ]);
})(window.angular);