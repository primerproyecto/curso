/* globals _ENV_ */
(function(angular) {

    /**
    * @ngdoc directive
    * @name component.incorporacion
    * @description
    * Componente para la incorporación de los ficheros
    */
    angular
        .module('app')
        .component('mainapp', {
            templateUrl: 'components/mainapp/mainapp-layout.html',
            controller: MainappController,
        });

    //incorporacionController.$inject = [];
    /**
    * @ngdoc method
    * @name incorporacionController
    * @methodOf component.incorporacion
    * @description
    * Controlador para el componente incorporación
    */
    function MainappController() {
        var ctrl = this;
    }
        
})(window.angular);
    