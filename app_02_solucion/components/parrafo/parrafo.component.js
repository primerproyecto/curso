/* globals _ENV_ */
(function(angular) {

    /**
    * @ngdoc directive
    * @name component.parrafo
    * @description
    * Componente para parrafo
    */
    angular
        .module('parrafo', [])
        .component('parrafo', {
            templateUrl: 'components/parrafo/parrafo-layout.html',
            transclude: true,
            controller: ParrafoController,
        });

    /**
    * @ngdoc method
    * @name ParrafoController
    * @methodOf component.parrafo
    * @description
    * Controlador para el componente parrafo
    */
    function ParrafoController() {
        var ctrl = this;
    }
        
})(window.angular);
    