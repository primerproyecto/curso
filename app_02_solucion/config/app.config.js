(function(angular) {
    'use strict';
    /* globals Confirming */
    //Inicializa módulo principal de la aplicación.
    var app = angular.module('app', []);

    //Lanza un promise para cargar los datos de la aplicación
    // y, cuando estén disponibles, lanzar el on ready de Angularjs, 
    // de forma análoga a como lo haríamos en **jQuery**.
    fetchData().then(bootstrapApplication);

    /**
    * @ngdoc function
    * @name fetchData
    * @description
    * Prepara el bootstrap de la aplicación llamando a los recursos de configuración.
    */
    function fetchData() {
        //Inyecta el módulo angular
        var initInjector = angular.injector(['ng']);
        //Inyecta $http para las llamadas asíncronas
        var $http = initInjector.get('$http');
        //devuelve el resultado de la llamada asíncrona tras llamar al
        //json indicado
        return $http.get('json/config.json').then(function(response) {
            var _constants = response.data;
            //Crea constante Angularjs`'configApp'
            angular.module('ng').constant('configApp', _constants);
        }, function(errorResponse) {
            //Devuelve un error, si se produce
            console.log('ERROR')
        });
    }

    /**
    * @ngdoc function
    * @name bootstrapApplication
    * @description
    * Lanza el bootstrap de la aplicación en `app`
    */
    function bootstrapApplication() {
        angular.element(document).ready(function() {
            //Lanza la aplicación cuando está todo disponible.
            angular.bootstrap(document, ['app']);
        });
    }
})(window.angular);
