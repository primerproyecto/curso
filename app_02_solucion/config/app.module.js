// jscs:disable
(function(angular) {
    'use strict';
    /**
    * @ngdoc overview
    * @name app
    * @description
    * Módulo principal que carga las dependencias
    */
    angular
    .module('app', [
        'app.core'
    ]).config(function($locationProvider, $httpProvider) {
    //    $locationProvider.html5Mode(true);
    })
    .run(function($state) {
        //La aplicación debe cargar un estado para ir a una página dentro del router
        $state.go('mainapp');
    });
})(window.angular);

