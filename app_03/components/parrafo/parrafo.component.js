/* globals _ENV_ */
(function(angular) {

    /**
    * @ngdoc directive
    * @name component.incorporacion
    * @description
    * Componente para la incorporación de los ficheros
    */
    angular
        .module('parrafo', [])
        .component('parrafo', {
            templateUrl: 'components/parrafo/parrafo-layout.html',
            transclude: true,
            controller: ParrafoController,
        });

    //incorporacionController.$inject = [];
    /**
    * @ngdoc method
    * @name incorporacionController
    * @methodOf component.incorporacion
    * @description
    * Controlador para el componente incorporación
    */
    function ParrafoController() {
        var ctrl = this;
    }
        
})(window.angular);
    