(function(angular) {
    angular
        .module('libro', [])
        .directive('libro', function() {
            return {
                restrict: 'AE',
                transclude: true,
                controllerAs: 'ctrl',
                bindToController: true,
                controller: LibroCtrl,
                link: LibroLink,
                scope: {
                    texto: '@'
                },
                templateUrl: 'components/libro/libro-layout.html',
            }
        });
        function LibroCtrl() {
            var ctrl = this;
            //definición de una propiedad en el controlador a modo de ejemplo.
            ctrl.color = 'red';
        }
        function LibroLink(scope, elem, attrs, controller) {
            //scope: scope de la directiva
            //elem: objeto DOM al que aplicar las modificaciones
            //attrs: Atributos del objeto
            //controller: controlador de la directiva, en este caso LibroCtrl.

            //Bind 'click'
            elem.bind('click', function() {
                elem.css('color', controller.color);
            });

            //Bind 'mouseover'
            elem.bind('mouseover', function() {
                elem.css('cursor', 'pointer');
            });
        }
})(window.angular);
