/* globals _ENV_ */
(function(angular) {

    /**
    * @ngdoc directive
    * @name component.incorporacion
    * @description
    * Componente para la incorporación de los ficheros
    */
    angular
        .module('titulo', [])
        .component('titulo', {
            templateUrl: 'components/titulo/titulo-layout.html',
            transclude: true,
            controller: TituloController,
        });

    //incorporacionController.$inject = [];
    /**
    * @ngdoc method
    * @name incorporacionController
    * @methodOf component.incorporacion
    * @description
    * Controlador para el componente incorporación
    */
    function TituloController() {
        var ctrl = this;
    }
        
})(window.angular);
    