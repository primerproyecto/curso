```shell
**Jorge Alarcón de Mena**
jorge.alarcon@entelgy.com
```
# ESTRUCTURA DE DIRECTIVAS

Las directivas son la pieza clave. Todo debe estructurarse, como sabes, en componentes. Un componente en Angularjs no es más que un helper de una directiva. La sintaxis es bastante complicada y es fácil perderse. Lo bueno es que su uso es más excepción (aunque habitual) que una norma.

Las directivas tienen un ciclo de vida complejo, `$compile`, `$controller` y `$link` son las más comunes y se utilizan en función del punto dentro del ciclo de vida del módulo en el que el código se utilice.

`$compile`: Transforma el DOM de un página antes de que sea renderizada finalmente. Como puedes suponer su uso no es muy común.

`$link`: Se invoca el código si `$compile` no está definido ya que se ejecutan en distintos puntos del ciclo de vida que los hacen incompatibles. Registra eventos y modifica el DOM, por lo tanto, se ejecuta cuando el DOM ya está disponible.

`$controller`: Es accesible por otras directivas y es donde se encuentran los datos de esta.

Las directivas no son objetos fáciles de entender. Utiliza siempre que puedas los componentes, son más sencillos, tanto estructuralmente como conceptualmente. Recurre a las primeras solo si debes manipular el DOM dentro de una estructura `$link`.


# EJERCICIO: 
El ejercicio que se plantea es muy sencillo. Simplemente queremos modificar la directiva para pasarle un color y el típico cursor con el dedito para los enlaces. Ya has visto cómo pasamos parámetros de tipo texto, ahora debes pasar un objeto de la manera `one-way-data binding`, como ya sabes que nos gusta.

## PISTA:
Los objetos `one direction` se envían como  **<** y los `two way direction` con **=**, prueba a utilizar ambos y luego hablamos de las diferencias con más detalle.

Puedes jugar a pasar todos los datos que quieras. Tendrás un componente cada vez más versátil y reutilizable.