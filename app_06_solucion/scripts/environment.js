var environment = {
    local: {
        mainApp: {
            method: 'GET',
            url: '/json/jsonLista.json'
        }
    },
    pr: {
        mainApp: {
            method: 'GET',
            url: 'URLPRODUCCION'
        }
    }
};

var _ENV_;
if (document.location.hostname === 'localhost' && document.location.port === '9000') {
    _ENV_ = environment.local;
} else {
   _ENV_ = environment.pr;
}