/* globals _ENV_ */
(function(angular) {

    /**
    * @ngdoc directive
    * @name component.lista-group
    * @description
    * Componente agrupoación de listado de libros
    */
    angular
        .module('listaLibros', [])
        .component('listaGrupo', {
            templateUrl: 'components/lista-grupo/lista-grupo-layout.html',
            transclude: true,
            controller: ListaGrupoController,
            bindings: {
                libros: '<'
            }
        });

    /**
    * @ngdoc method
    * @name ListaGrupoController
    * @methodOf component.listado
    * @description
    * Controlador para el componente listado
    */   
    ListaGrupoController.$inject = ['$injector', '$attrs'];
    function ListaGrupoController($injector, $attrs){
        var ctrl = this;
    }

})(window.angular);