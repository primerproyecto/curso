/* globals _ENV_ */
(function(angular) {

    /**
    * @ngdoc directive
    * @name component.incorporacion
    * @description
    * Componente para la incorporación de los ficheros
    */
    angular
        .module('app')
        .component('mainapp', {
            templateUrl: 'components/mainapp/mainapp-layout.html',
            controller: MainappController,
        });

    /**
    * @ngdoc method
    * @name incorporacionController
    * @methodOf component.incorporacion
    * @description
    * Controlador para el componente incorporación
    */
    MainappController.$inject = ['dataservice', 'configApp'];
    function MainappController(dataservice, configApp) {
        var ctrl = this;
        
        ctrl.userForm = {};
        ctrl.submitForm = function(form) {
            ctrl.libros.push(
                { autor: ctrl.userForm.autor, titulo: ctrl.userForm.titulo }
            );      
            // Limpia
            ctrl.userForm.autor = '';
            ctrl.userForm.titulo = '';
            /*
            data = ctrl.userForm;
            dataservice.ajaxRequest(_ENV_.mainAppList.url, _ENV_.mainAppList.method, data, configApp).then(function(response) {
                console.log('orden guardada con éxito')
            },
            function(response) {
                console.log('ERROR 500');
                si.ctrl.errorServer = true;
            });*/
        }
        ctrl.loadContent = function(){
            //inicializamos 'data', como no es necesario mandar nada al servidor, esta variable queda
            //vacía
            var data = '';

            //(pensemos en encapsular este método dentro de un servio que lo aplique, de manera que la implementación particular de la llamada
            //no suponga una dependencia externa)
            //Invocación al servicio que hemos inyectado para la carga de los datos haciendo uso del objeto
            //de configuración
            dataservice.ajaxRequest(_ENV_.mainApp.url, _ENV_.mainApp.method, data, configApp).then(function(response) {
            //Asignamos la colección a la variable sobre la que iteraremos.
                ctrl.libros = response.data.libros;
            },
            function(response) {
                console.log('error');
            });
        }
        ctrl.loadContent();
    }
})(window.angular);
    