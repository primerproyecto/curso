/* globals _ENV_ */
(function(angular) {

    /**
    * @ngdoc directive
    * @name component.about
    * @description
    * Componente información sobre 'acerca de'
    */
    angular
        .module('app')
        .component('about', {
            templateUrl: 'components/about/about-layout.html',
            controller: AboutController,
        });

    /**
    * @ngdoc method
    * @name incorporacionController
    * @methodOf component.about
    * @description
    * Controlador para el componente 'about'
    */
    AboutController.$inject = [];
    function AboutController() {
        var ctrl = this;

    }
})(window.angular);
    