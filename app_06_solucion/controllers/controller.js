var app = angular.module('app', []);

app.controller('EscritoresListadoCtrl', function ($scope) {
    $scope.model = [
        { nombre: 'Manuel', apellido: 'Machado'},
        { nombre: 'Jorge Luis', apellido: 'Borges'}
    ];
    $scope.registrar = function(){
	    if(typeof($scope.nombre) !== 'undefined' && typeof($scope.apellido) !== 'undefined'){
	        $scope.model.push(
	            { nombre: $scope.nombre, apellido: $scope.apellido }
	        );      
	        // Limpia
	        $scope.marca = null;
	        $scope.nombre = null;
	        $scope.precio = null;
	    }
	   
	    return false;
	}
	$scope.eliminar = function($index){
    	$scope.model.splice($index, 1);
	}
});
