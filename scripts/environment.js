var environment = {
    local: {
        mainApp: {
            method: 'GET',
            url: '/json/jsonLista.json'
        }
    },
    pr: {
        mainApp: {
            method: 'GET',
            url: 'URLPRODUCCION'
        }
    }
};

var _ENV_;
if ((document.location.hostname === 'localhost' && document.location.port === '9000') || (document.location.hostname === '127.0.0.1' && document.location.port === '8887')) {
    _ENV_ = environment.local;
} else {
   _ENV_ = environment.pr;
}