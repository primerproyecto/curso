```shell
**Jorge Alarcón de Mena**
jorge.alarcon@entelgy.com
```

# FILTROS Y WATCHERS

En esta última unidad vamos a mezclar un concepto muy sencillo con otro bastante más complicado.

El concepto sencillo son los filtros. El que utilizamos aquí es un filtro que Angularjs nos proporciona para poder rebuscar sobre el resultado de una iteración aplicada sobre una colección.

El concepto más complicado son los `watchers`. Seguro que piensas que debería bastar con una asignación y ya está. Bueno, eso vale si estamos en el mismo componente, pero aquí estamos pasando ese array recogido del input de filtrado a un componente y de ahí, mediante `require` lo estamos utilizando en `lista-elemento` por lo que no tiene medio el hijo de saber que el padre ha cambiado. Por eso utilizamos un `watcher`, observamos el cambio y lo actualizamos. Verás que hemos de inyectar `$scope` para que funcione. Son dependientes, como el `$scope`es dependiente del controlador sobre el que se inyecta tampoco es mucho problema. Solo lo utilizamos para esto, no para pasar modificar datos en sitios donde no toca. Ah, me acabo de acordar, a este problema derivado de andar para arriba y para abajo con los scopes y no saber ya dónde se hace qué ni quién se responsabiliza de qué (¡anarquía!) se le llama "scope soup", o sea, "sopa de scopes", un jaleo de no te menees imposible de mantener. Lo único más triste que esto fue un comentario que leí hace unos cuatro o cinco años: `//Chapuza para que funcione esto, fallará en otros casos, arreglar lo antes posible 12/06/2004//`. La vida da mucha pena en ocasiones.


# EJERCICIO: 
Como ha sido una lección dura vamos a limitarnos a crear el componente de búsqueda. Piensa en un componente como en un bloque reutilizable, por lo tanto evita dependencias no personalizables que impidan que lo sea. 

##Pista:
Siempre que trabajemos con `ngModel` (o sea, formularios) la relación es u`n `two-way-databinding` y si pasamos parámetros siempre "=". Esto es así para asegurar que el valor definido en `$modelValue`sea consistente siempre con el presentado en `$viewValue`.
