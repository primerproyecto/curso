(function(angular) {

    /**
    * @ngdoc directive
    * @name component.filtrado
    * @description
    * Componente para filtrado
    */
    angular
        .module('filtrado', [])
        .component('filtrado', {
            templateUrl: 'components/filtrado/filtrado-layout.html',
            transclude: true,
            controller: FiltradoController,
            bindings: {
                texto: '@',
                filterLibros: '='
            }
        });

    //FiltradoControllerController.$inject = [];
    /**
    * @ngdoc method
    * @name FiltradoController
    * @methodOf component.filtrado
    * @description
    * Controlador para el componente filtrado
    */
    function FiltradoController() {
        var ctrl = this;
        
    }
        
})(window.angular);
    