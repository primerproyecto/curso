(function(angular) {
    'use strict';
    /**
    * @ngdoc directive
    * @name component.app
    * @description
    * Componente que define la entrada a la aplicación
    */
    angular
        .module('app')
        .config(configIncorpRoute)
        .component('app', {
            templateUrl: 'components/app/app-layout.html'
        });

    /**
    * @ngdoc object
    * @name app.config.routes
    * @requires $stateProvider
    * @description
    * Routing principal de la aplicación
    */
    function configIncorpRoute($stateProvider) {
        $stateProvider
            .state('mainapp', {
                url: '/mainapp',
                template: '<mainapp></mainapp>'
            })
            .state('about', {
                url: '/about',
                template: '<about></about>'
            });
    }
})(window.angular);