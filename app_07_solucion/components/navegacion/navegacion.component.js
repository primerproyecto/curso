/* globals _ENV_ */
(function(angular) {

    /**
    * @ngdoc directive
    * @name component.navegacion
    * @description
    * Componente principal para la gereración de la navegacion
    */
    angular
        .module('navegacion', [])
        .component('navegacion', {
            templateUrl: 'components/navegacion/navegacion-layout.html',
            controller: NavegacionController
        });

    /**
    * @ngdoc method
    * @name NavegacionController
    * @methodOf component.navegacion
    * @description
    * Controlador para el componente navegacion
    */   
    NavegacionController.$inject = ['$injector', '$attrs', 'navigation'];
    function NavegacionController($injector, $attrs, navigation) {
        var ctrl = this;
        ctrl.goNextState = function(whereToGo) {
            navigation.goNextState(whereToGo);
        }
        ctrl.getClass = function(path) {
            return navigation.getClass(path);
        }
    }

})(window.angular);