/* globals _ENV_ */
(function(angular) {

    /**
    * @ngdoc directive
    * @name component.about
    * @description
    * Componente información sobre 'acerca de'
    */
    angular
        .module('app')
        .component('about', {
            templateUrl: 'components/about/about-layout.html',
            controller: AboutController,
        });

    /**
    * @ngdoc method
    * @name incorporacionController
    * @methodOf component.about
    * @description
    * Controlador para el componente 'about'
    */
    AboutController.$inject = ['$state'];
    function AboutController($state) {
        var ctrl = this;
        ctrl.goNextState = function (whereToGo) {
            switch (whereToGo) {
                case 'mainapp': 
                    $state.go('mainapp');
                    break;
                case 'about': 
                    $state.go('about');
                    break;
                default:
                    $state.go('mainapp');
            }
        }
    }
})(window.angular);
    