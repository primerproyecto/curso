/* globals _ENV_ */
(function(angular) {

    /**
    * @ngdoc directive
    * @name component.listado
    * @description
    * Componente principal para la gereración del listado de libros
    */
    angular
        .module('listado', [])
        .component('listado', {
            templateUrl: 'components/listado/listado-layout.html',
            controller: ListadoController,
            bindings: {
                'libros': '<'
            }
        });

    /**
    * @ngdoc method
    * @name ListadoController
    * @methodOf component.listado
    * @description
    * Controlador para el componente listado
    */   
    ListadoController.$inject = [];
    function ListadoController(){
        var ctrl = this;
    }

})(window.angular);