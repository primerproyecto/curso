```shell
**Jorge Alarcón de Mena**
jorge.alarcon@entelgy.com
```

# NAVEGACIÓN y ENRUTACIÓN
Angularjs recomienda en su documentación acerca de la convergencia conceptual teórica con **Angularjs 2** el servicio de routing proporcionado por `Angular UI Routing`, hasta tal punto que se ha convertido en un estándar de facto.

Algo hemos visto ya en `config/app_module.js`, allí hicimos uso de una redirección forzada para que entrase siempre en el estado `mainapp` que establecimos como punto de entrada con `$state.go('mainapp');`.

Para definir los estados nos hemos complicado un poco la vida creando un método en el controlador en vez de utilizar algo como  `<a ui-sref="about">About</a>`. En nuestro contexto no tiene mucha utilidad, pero podría resultar de valer para establecer un sistema de enrutamiento compartido entre distintas partes de la aplición con opciones que fueran más allá de un simple `switch`.

Creamos un nuevo estado `about` que contendrá la misma barra de navegación y un contenido cualquiera para navegar entre `mainapp`y `about`.

No deberías tener ningún problema para entender lo que hemos visto hasta ahora. Probemos con un práctica.


# EJERCICIO: 
Vamos a hacer varias cosas aquí:

1/ Externalizar el componente de navegación para que pueda ser tomado como pastilla y utilizarlo en todos los estados de mi aplicación.

2/ Crea un servicio de navegación `navigation.service.js` que tendrá tanto el método de navegación como estados como una función que devolverá si un elemento de menú está activo o no`, utiliza algo así.

```shell
        function getClass(path) {
            return ($location.path().indexOf(path) > -1) ? 'active' : '';
        }
```
La estructura del servicio es idéntica a nuestro conocido `dataservices.js`.

Podrías tener la tentación de hacer algo como `<li ui-sref-active="active" class="item">`. No solo funcionaría sino que sería la opción idónea, pero la práctica perdería mucha gracia. Haz primero lo que te propongo, aunque sea echando un vistazo (un vistazo, no un copiar y pegar) y después prueba lo anterior, la diferencia se nota.



