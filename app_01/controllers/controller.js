// Se declara la aplicación con el nombre 'app'
var app = angular.module('app', []);

//Controlador para el módulo definido en la línea anterior.
//Le pasamos como parámetro '$scope'
app.controller('EscritoresListadoCtrl', function ($scope) {

//definimos un array de objetos
    $scope.model = [
        { nombre: 'Manuel', apellido: 'Machado'},
        { nombre: 'Jorge Luis', apellido: 'Borges'}
    ];

//Método que añadirá escritores al array $scope.model
    $scope.registrar = function(){
        // comprobamos que los datos están definidos.
        if(typeof($scope.nombre) !== 'undefined' && typeof($scope.apellido) !== 'undefined'){
        //Lo añadimos nuestro array            
            $scope.model.push(
                { nombre: $scope.nombre, apellido: $scope.apellido }
            );      
            // Limpiamos las variables para que se borre su contenido.
            $scope.nombre = null;
            $scope.apellido = null;
        }
    }
});
