```shell
**Jorge Alarcón de Mena**
jorge.alarcon@entelgy.com
```

# CURSO ENTELGY ANGULARJS/BOOTSTRAP

##Introducción
Este curso se divide en seis unidades que explican una característica fundamental de Angularjs. Esas características pueden componerse de más de un elemento, pero siempre se trata de que se apoyen en unidades precedentes con el fin de afianzar conocimientos aprendidos. Tienes a tu disposión las unidades y el material para dedicarle el tiempo que quieras.  Si tienes cualquier duda, ponte en contacto conmigo. Algo podremos hacer.

## Cómo trabajar
Antes de nada, si no te has instalado el entorno de trabajo, en la parte inferior de este documento tienes un enlace de Arquitectura Front para guiarte en el proceso.

Si ya lo tienes todo, descárgate el repositorio, para ello ejecuta:

```shell
En el directorio elegido:
git init
git remote add origin https://gitlab.com/entelgy/curso.git
git pull origin master
npm install
bower install --save
```

Para ver cada unidad debes renombrar la que elijas a `app`.

## Unidades temáticas
A lo largo de este curso recorreremos las principales funcionalidades tal y como se aplicarían en el mundo real. No pretende ser un conjunto de "mejores prácticas", solo de "buenas". A partir de ahí todo es debatible.

A continuación te resumo de qué trata cada unidad, cada una de ella ofrece la solución al ejercicio planteado, los encontrarás en **unidad_solucion**.

### `app_01`:
Introducción a conceptos básicos: No esperes nada revolucionario. La ayuda está bastante bien, échale un vistazo.

### `app_02`:
Bootstrap de la aplicación y componentes básicos. Aquí empieza lo divertido. A partir de ahora desarrollaremos como Dios manda. Comenzaremos a aplicar los principios que guiarán el conjunto de estas prácticas.

### `app_03`:
Planteamos un ejemplo de directiva. Las directivas son la Madre del Cordero. Es el sitio donde debes plantearte manipular el DOM; si es que necesitas hacerlo que en la mayoría de los casos crees que sí, pero es que no.

### `app_04_01`:
La unidad 4 es amplia, por eso la dividimos en dos. En esta veremos conceptos avanzados muy interesantes y que se utilizan una barbaridad: DIs (Inyecciones de DependenciaS), servicios promise (servicios en general, pero es que estos los vas a usar hasta hartarte y más allá), carga de datos e iteración como producto del resultado del servicio de datos.

### `app_04_02`:
Lo normal cuando se cocina un plato elaborado es dejar la cocina como el Delta del Mekong. Aquí la limpiamos refactorizando lo desarrollando en el punto anterior y preparando la aplicación para la siguiente práctica. Veremos el modelo top>down de comunicación entre web components que es como se trabaja de verdad. Angularjs no nació con estas premisas, pero se ha adaptado admirablemente en lo que va de año. Las cosas cambian rápido.

### `app_05`:
Formularios y validaciones. Qué sería de una aplicación sin formularios... probablemente una aplicación más feliz. Ya que hemos de enfrentarnos a ellos, por lo menos Angularjs nos ofrece una forma consistente y sencilla de hacerlo. Esta es la única aplicación sin práctica; qué suerte tienes, practica, prueba, juega y equivócate un rato.

### `app_06`:
Routing. Hemos visto tantas cosas que nos dejábamos la navegación de lado. En realidad no, era preferible dominar conceptos elementales antes que ver esto. Te sonará a muchas cosas que has visto porque esencialmente es lo mismo. No es dificil. Aquí utilizamos `Angularjs UI Router` que es el estándar de facto, incluso por encima del sistema propio de Angularjs. La práctica es de las más divertidas, la haremos poco a poco y lo que nos salga, sin presión.

### `app_07`:
Solo nos queda por ver los filtros. Lo vas a entender a la primera porque no tiene dificultad. Fíjate cómo lo hemos construido y montado todo.

## Recursos
Información y enlaces relacionados. Tanto a documentación de elaboración propia como a recursos de terceros. Si te interesa cada enlace te proporcionará horas de placer.

###  Arquitectura Front
https://docs.google.com/document/d/1RlQo0tpLqtGqkbUT8WcE7shRUIHNDRJ-jrE01zjiWMA/edit?usp=sharing

### Buenas prácticas Front
https://docs.google.com/document/d/1MtrbmWD20RPg9U9EWmH65WLUzB5loxQkZJ6-CQI407Q/edit?usp=sharing

### Bootstrap
http://getbootstrap.com/

### SASS
http://sass-lang.com/

### Yeoman
http://yeoman.io/

### Grunt
http://gruntjs.com/

### Bower
https://bower.io/